import java.io.File;
import java.nio.channels.FileChannel;

/*
 * Use thie class to stores files that currently be opened
 * */
public class OpeningFileInfo {
	public String filePath;
	public FileChannel fileChannel;
	public boolean isOpenForWrite;
	
	public OpeningFileInfo(String filePath, FileChannel fileChannel, boolean isOpenForWrite){
		this.filePath = filePath;
		this.fileChannel = fileChannel;
		this.isOpenForWrite = isOpenForWrite;
	}
}
