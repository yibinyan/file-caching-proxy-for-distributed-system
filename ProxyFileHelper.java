import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;


public class ProxyFileHelper {
	public final static int BUF_SIZE = 1024 * 1024 + 256; /* size of buffer */
	public ServerFile serverFile;	/* Instance of remote object */
	public final static int FILENOTFOUND_ERROR = -1;
	public final static int EIO = -5;	/* I/O error */
	public final static int EFBIG = -27; /* file too big */
	public final static int EOVERFLOW = -75; /* file too big */
	public final static int EACCES = -13; /* Permission denied */
	
	public ProxyFileHelper(ServerFile serverFile) {
		this.serverFile = serverFile;
	}
	
	/* check whether the current version of cached file is up-to-date */
	public boolean checkVersion(String filename, int localVersion){
		try {
			Integer serverVersion = serverFile.getServerCopyVersion(filename);
			if (serverVersion == null){
				serverVersion = -1;
			}
			
			System.out.println("[Proxy] localVersion: " + localVersion);
			System.out.println("[Proxy] serverVersion: " + serverVersion.intValue());
			return localVersion == serverVersion.intValue();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return false;
	}
	
	/* download from server and update version of cached file */
	public int downloadAndUpdate(int clientID, String path){
		int result = 0;
		
		if ((result = downloadFromServer(clientID, path)) >= 0){
			/* download successfully, return the new version number */
			try {
				return serverFile.getServerCopyVersion(path);
			} catch (FileNotFoundException e){ 
				return FILENOTFOUND_ERROR;
			} catch (IOException e) {
				System.out.println("[downloadAndUpdate error]");
				return EIO;
			}
		}else{
			return result;
		}
	}
	
	/* download file from server */
	public int downloadFromServer(int clientID, String path){
		int downloadSize = 0;
		FileOutputStream fos = null;
		File f = null;
		
		try {
			File temp = new File(Proxy.cacheDir, path);
			File parent = temp.getParentFile();
			parent.mkdirs();
			
			fos = new FileOutputStream(temp);
			
			byte[] buf;
			downloadSize = 0;
			String downloadID = null;
			DownloadFormat df;
			
			/* when first call serverFile.download, client should pass null downliad ID to server.
			 * And server would allocate a download ID for client to trace */
			while ((df = serverFile.download(clientID, path, downloadID)) != null){
				buf = df.datas;
				downloadID = df.downloadID;
				while(downloadSize + buf.length > Proxy.cache.remainSize){
					long releasedSize = Proxy.cache.evict();
					if (releasedSize < 0){
						/* cache is empty, file is too large */
						fos.close();
						f = new File(Proxy.cacheDir, path);
						if ((f != null) && (f.exists())){
							f.delete();
							serverFile.stopDownload(clientID, path);
						}
						return EFBIG;
					}
				}
				
				fos.write(buf);
				if (df.isFinalPart){
					System.out.println("[downloadFromServer] Final Part");
					break;
				}
			} 
			
			fos.close();
			
		} catch (FileNotFoundException e){
			f = new File(Proxy.cacheDir, path);
			
			try {
				if (fos != null){
					fos.close();
				}
				System.out.println("[downloadFromServer] Closing Outputstream");
			} catch (IOException e2) {
				e2.printStackTrace();
			}
			
			if ((f != null) && (f.exists())){
				f.delete();
				try {
					serverFile.stopDownload(clientID, path);
					System.out.println("[downloadFromServer] File is not exist in Server");
				} catch (IOException e1) {
					System.out.println("Error happen when stop downloading");
					e1.printStackTrace();
				}
			}

			return FILENOTFOUND_ERROR;
		} catch (IOException e) {
			try {
				if (fos != null){
					fos.close();
				}
				System.out.println("[downloadFromServer] Closing Outputstream");
			} catch (IOException e2) {
				e2.printStackTrace();
			}
			
			f = new File(Proxy.cacheDir, path);
			if ((f != null) && (f.exists())){
				f.delete();
				try {
					serverFile.stopDownload(clientID, path);
					e.printStackTrace();
				} catch (IOException e1) {
					System.out.println("Error happen when stop downloading");
					e1.printStackTrace();
				}
			}
			
			return EIO;
		}
		
		return downloadSize;
	}
	
	/* upload file to the server */
	public boolean uploadToServer(int clientID, String path){
		try {
			FileInputStream fis = new FileInputStream(new File(Proxy.cacheDir, path));
			
			int byteread = 0;
			byte[] buf = new byte[BUF_SIZE];
			
			while ((byteread = fis.read(buf)) != -1){
				byte[] result = Arrays.copyOf(buf, byteread);
				serverFile.upload(clientID, path, result);
			}
			
			/* set buf as null in order to close the output stream in server */
			serverFile.upload(clientID, path, null);
			fis.close();
			
		} catch (IOException e) {
			System.out.println("Upload Error");
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	/* upload private write copy to server */
	public boolean uploadPrivateCopy(int clientID, String path){
		try {
			FileInputStream fis = new FileInputStream(new File(Proxy.cacheDir, path));
			
			int byteread = 0;
			byte[] buf = new byte[BUF_SIZE];
			
			/* modify file name since its private copy */
			int index 
				= path.lastIndexOf("_private_");
			String filename = path.substring(0, index);
			System.out.println("[uploadPrivateCopy] " + filename);
			
			while ((byteread = fis.read(buf)) != -1){
				byte[] result = Arrays.copyOf(buf, byteread);
				serverFile.upload(clientID, filename, result);
			}
			
			/* set buf as null in order to close the output stream in server */
			serverFile.upload(clientID, filename, null);
			fis.close();
			
		} catch (IOException e) {
			System.out.println("Upload Error");
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	/* delete file in server */
	public int deleteServerFile(String path){
		int result = 0;
		
		try {
			result = serverFile.deleteFile(path);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return result;
	}
}
