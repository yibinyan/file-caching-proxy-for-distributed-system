import java.io.File;
import java.io.IOException;
import java.rmi.AlreadyBoundException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;

/*
 * Class of server
 * */

public class Server {
	public static int port;	/* port that server would use */
	public static String rootDir; /* root directory of server */
	/* use HashMap to store versions of file*/
	public static HashMap<String, Integer> filesVersion;  
	public static int BUF_SIZE = 1024 * 1024 + 256; /* max size of  buffer */

	public static void main(String[] args) {
		if (args.length != 2) {
			System.out.println("Server Argument Error");
			return;
		}

		Registry rmiRegistry;
		filesVersion = new HashMap<String, Integer>();
		port = Integer.parseInt(args[0]);
		try {
			rootDir = new File(args[1]).getCanonicalPath();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		try {
			initial();
			rmiRegistry = LocateRegistry.createRegistry(port);
			rmiRegistry.bind("server", new ServerFileImpl());
			System.out.println("Server started, Port = " + port);

		} catch (RemoteException e) {
			e.printStackTrace();
		} catch (AlreadyBoundException e) {
			e.printStackTrace();
		}

	}

	/* initial all file versions */
	public static void initial() {
		getFiles(rootDir);
	}
	
	/* Scan files in the root directory and initailize it */
	public static void getFiles(String rootDir){
		File root = new File(rootDir);
		File[] files = root.listFiles();
		
		for (File file : files) {
			if (file.isDirectory()) {
				getFiles(file.getAbsolutePath());
			} else {
				filesVersion.put(file.getAbsolutePath(), 0);
			}
		}
	}

}