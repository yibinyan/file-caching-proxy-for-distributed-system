# README #

## Project Overview ##     
* Developed a proxy that handles the RPC file operation request from client interposition library. The proxy supported LRU caching. It would fetch whole files from server through RMI, and cache them locally.
* Created a server that stores and provides access to a set of files. It operated at the granularity of files and handled concurrent connection from multiple proxies.
* Implemented Open/Close (Fetch-Update) Consistency Model, which is a form of session semantics.

## Details ##
The consistency model I implement in this project is Open-Close (fetch-update) model. When a client opens a file, it will compare version of cached file in its directory with the version of file in server (This ensure the freshness of cache before open). If the file is not exist or is outdated, proxy would fetch file from server and cache it. Once opened, file would act like a private copy to client. On close, if file is changed, the new version would be pushed to server.    

###Some explain of the word I would use###
1. Version:
I keep recording version of file in proxy (version of cached file) and server. And I regard deleting as an update of version.
2. Fetch:
When I say fetch, it means make the cache file exactly the same as what it is in server. So, if the newest version of file is deleted in server, proxy would delete its corresponding cached file and tell client that file is not found.

###Things I did to ensure open-close semantics###
* On Client and Proxy side   
** When open for read (that is READ):  **  
If cached file is outdated, then we should check whether there is other clients still opening this file.   
-If YES, then I still keep this stale file but make it as a private copy shared by those clients (By modifying file name and corresponding cache). And proxy will fetch the new version of file from server.     
-If NO, then I would delete this file from cache directly.    
**When open for write (that is WRITE/CREATE/CREATE_NEW): **    
Use private copy for every client. And opened private copy would be put in cache. On close, proxy would upload the private copy to server, and update the corresponding file.    
    
* On Server side  
**When client try to fetch (download) file from server:**    
Server would make a temporary copy of target file, and proxy actually read the copy. The reason for doing this is that there may be multiple proxies downloading and updating the file we are downloading. This would probably lead to some corrupted content. When a client start downloading a file, client should be able to regard this file as private copy and content of this file should never change. So, the best way to do this is let client read the temporary file and server would delete after downloading.      
**When client try to update (upload) file to server:**     
Server would make a temporary copy too. Since reading the data from proxy through RMI is not one-time operation, server would keep writing file until it get the last part of the source file. The content of file would keep changing during this time, if we just modify the original file directly, some client would read an unfinished file (part of file is updated but server is still writing it). So I choose to create copy for writing. Server will replace the original file with temprory one after writing.
     

###Protocols###
* Download      
RMI function: DownloadFormat download(int clientID, String filename, String downloadID)     
**Arguments: **         
| ClientID 	| FILENAME 	| DOWNLOAD ID 	|     
*ClientID(4 bytes):* Each client would have its own ID when get file handler. This stands for its identification.
FileName(String): Unique filename that clients wants to download.      
*DOWNLOAD ID(4 bytes): *Client should call download multiple times in order to get the whole file. The first time client call download the downloadID would be null. Server will allocate a downloadID for clients in the return value of this function.      
**Return value: **     
*DownloadFormal Class*    
| Download ID 	| IS FINAL PART OF FILE 	| DATA ... 	|     
*DOWNLOAD ID(4 bytes)*: Server will allocate a unique download ID for clients. Client should use download ID as argument next time it call download in order to download remain parts of target file.     
*IS FINAL PART OF FILE (boolean)*: Server would mark the result data as final part of target file if file was read completely.     
*DATA*: partial data of the target file. Client should keep collect it until download complete file.           

* Upload      
RMI function: upload(int clientID, String filename, byte[] buf)         
**Arguments: **    
| ClientID 	| FILENAME   | DATA ...   |     
*ClientID(4 bytes)*: Each client would have its own ID when get file handler. This stands for its identification.    
*FileName(String)*: Unique filename that clients wants to update.    
*DATA:* partial data of the source file. When data is null, server knows that clients finish uploading.    

###LRU Cache###
Use a double linked list to store all the states of cached file. State of File that is used most recently would be add to the tail of list. (Also I use a Hashmap to store nodes, so I can find node much quickly.)     
While evicting cache, I would check nodes start with the head of list. If the cache is not open by any clients (I would keep the number of clients who is opening the file in the node), just evict this node and delete corresponding cached files. If not, just find the next node.