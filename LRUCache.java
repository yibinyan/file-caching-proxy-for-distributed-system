import java.io.File;
import java.util.Hashtable;
import java.util.Map;

/*
 * LRU Cache
 * 
 * */

public class LRUCache {
	CacheFileNode head; /* head of cache */
	CacheFileNode tail; /* tail of cache */
	long currentSize; /* current size of cache */
	long remainSize;  /* remain size of cache */
	Hashtable<String, CacheFileNode> nodeTable; /* hash table that helps to find node easilier */
	
	public LRUCache(){
		head = new CacheFileNode();
		tail = head;
		currentSize = 0;
		remainSize = Proxy.cacheSize;
		nodeTable = new Hashtable<String, CacheFileNode>();
	}
	
	/* get corresponding node from cache by file path */
	public CacheFileNode get(String path){
		CacheFileNode node = nodeTable.get(path);
		if (node != null){
			return node;
		}else{
			return null;
		}
	}
	
	/* When file turn stale but someone is still opening, 
	 * turn it to private copy*/
	public CacheFileNode changeNameAndTurnStale(String oldPath, String newPath){
		CacheFileNode node = nodeTable.get(oldPath);
		if (node != null){
			node.fileName = newPath;
			node.isStale = true;
			nodeTable.remove(oldPath);
			nodeTable.put(newPath, node);
			
			return node;
		}else{
			return null;
		}
	}
	
	
	/* append file to the end of cache when file is visited */
	public boolean update(String path, long newFileSize){
		System.out.println("[Cache update] " + path);
		CacheFileNode node = nodeTable.get(path);
		if (node != null){
			
			if (node == tail){
				tail = tail.before;
			}
			
			/* recently used, add in into the tail */
			long sizeChange = newFileSize - node.fileSize;
			node.before.deleteNext();
			node.fileSize = newFileSize;
			
			tail.addToNext(node);
			tail = tail.next;
			nodeTable.put(path, node);
			
			currentSize = currentSize + sizeChange;
			remainSize = remainSize - sizeChange;
			
			return true;
		}else{
			return false;
		}
	}
	
	/* add node to the cache */
	public void addToCache(String path, long size, int fileVersion){
		
		CacheFileNode newNode = new CacheFileNode(path, size, fileVersion);
		tail.addToNext(newNode);
		tail = tail.next;
		currentSize = currentSize + size;
		remainSize = remainSize - size;
		nodeTable.put(path, newNode);
		
		System.out.println("[ADD TO CACHE] " + path);
		
		return;
	}
	
	/* return the size of file evicted*/
	public long evict(){
		CacheFileNode temp = head;
		CacheFileNode tempNext = head.next;
		
		while ((tempNext != null) && (tempNext.openCount != 0)){
			temp = tempNext;
			tempNext = temp.next;
		}
		
		if (tempNext != null){
			if (tempNext == tail){
				tail = tail.before;
			}
			
			temp.deleteNext();
			/* updata current cache size */
			currentSize = currentSize - tempNext.fileSize;
			remainSize = remainSize + tempNext.fileSize;
			/* remove file from cache */
			File f = new File(Proxy.cacheDir, tempNext.fileName);
			f.delete();
			nodeTable.remove(tempNext.fileName);
			/* return released space */
			return f.length();
		}else{
			/* return -1 when cache is empty */
			return -1;
		}
	}
	
	/* delete specific node */
	public void deleteFromCache(String fileName){
		
		CacheFileNode node = nodeTable.get(fileName);
		if (node != null){
			if (node == tail){
				tail = tail.before;
				System.out.println("[DeleteFromCache] " + " IS TAIL");
			}
			
			/* remove file from cache */
			CacheFileNode nodeBefore = node.before;
			nodeBefore.deleteNext();
			/* update current cache size*/
			currentSize = currentSize - node.fileSize;
			remainSize = remainSize + node.fileSize;
			/* delete file */
			File f = new File(Proxy.cacheDir, node.fileName);
			f.delete();
			System.out.println("[DeleteFromCache] " + fileName);
			nodeTable.remove(fileName);
		}

		return;
	}
	
	/* print out all content of current cache */
	public void printCache(){
		System.out.println("Current Cache : ");
		CacheFileNode n = head.next;
		int count = 1;
//		System.out.println("HashTableSize: " + nodeTable.size() + " " + n);
		while (n != null){
			System.out.println(count + ". " + n.fileName + " version " + n.fileVersion);
			count++;
			n = n.next;
		}
	}
}
