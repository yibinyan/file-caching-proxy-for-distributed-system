import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.rmi.Remote;

/*
 * interface of ServerFile 
 * */

public interface ServerFile extends Remote{
	/* just for test*/
	public String sayHello() throws IOException;
	
	/* client would call this methods to upload file 
	 * clientID : each clients would have its own ID
	 * filename : name of files in the server that clients want to update 
	 * buf : partial data of source file */
	public void upload(int clientID, String filename, byte[] buf) throws IOException;
	
	/* client would call this methods to download file 
	 * clientID : each clients would have its own ID
	 * filename : name of files in the server that clients want to download 
	 * downloadID : download ID that server allocate for clients, 
	 * 				when first time client call this method, it should be null */
	public DownloadFormat download(int clientID, String filename, String downloadID)  throws FileNotFoundException, IOException;
	
	/*
	 * Stop downloading. It is used when error happens. 
	 * clientID : each clients would have its own ID
	 * filename : name of files in the server that clients want to stop downloading 
	 * */
	public void stopDownload(int clientID, String filename) throws IOException;
	
	
	/*
	 * delete file in server.  
	 * filename : name of files in the server that clients want to delete 
	 * */
	public int deleteFile(String filename) throws IOException;
	
	/*
	 * Get version of server file  
	 * filename : name of files in the server that clients want to get version 
	 * */
	public Integer getServerCopyVersion(String filename) throws IOException;
}
