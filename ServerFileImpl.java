import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.nio.file.Files;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.concurrent.Semaphore;


/*
 * Implementation of remote object
 * */
public class ServerFileImpl extends UnicastRemoteObject implements ServerFile, Serializable {
	/* Hashmap that stores inputstream of downloading files */
	Hashtable<String, FileInputStream> inputHash; 
	/* Hashmap that stores outputstream of uploading files */
	Hashtable<String, FileOutputStream> outputHash;
	/* client ID who owns the upload lock */
	int uploadingClientID;
	
	public ServerFileImpl() throws RemoteException {
		super();
		inputHash = new Hashtable<String, FileInputStream>();
		outputHash = new Hashtable<String, FileOutputStream>();
		uploadingClientID = -1;
	}

	@Override
	public String sayHello() throws IOException {
		return "SERVER: Hello";
	}
	
	/*
	 * Get version of server file  
	 */
	public Integer getServerCopyVersion(String filename){
		File f = new File(Server.rootDir, filename);
		return Server.filesVersion.get(f.getAbsolutePath());
	}
	
	
	/* server call this methods to update file version */
	private void updateServerCopyVersion(String filename){
		File f = new File(Server.rootDir, filename);
		Integer oldVersion = Server.filesVersion.get(f.getAbsolutePath());
		int newVersion;
		
		if (oldVersion == null){
			newVersion = 0;
		}else{
			newVersion = oldVersion + 1;
		}
		
		Server.filesVersion.put(f.getAbsolutePath(), newVersion);
	}
	
	/* client would call this methods to download file */
	public DownloadFormat download(int clientID, String filename, String downloadID) throws FileNotFoundException, IOException{

		File f = new File(Server.rootDir, filename);
		int count = 0;
		File tempFile = new File(Server.rootDir, filename + "@" + clientID + "_" + count);
		
		synchronized(this){
			if (downloadID == null){
				while (tempFile.exists()){
					count++;
					tempFile = new File(Server.rootDir, filename + "@" + clientID + "_" + count);
				}
			
				downloadID = "" + count;
			}
		
			if (!f.exists()){
				throw new FileNotFoundException();
			}
		
			if (!tempFile.exists()){
				Files.copy(f.toPath(), tempFile.toPath());
			}
		}
		
		/* each client would have its own FileInputStream */
		String fullname = tempFile.getAbsolutePath();
		FileInputStream fis; 
		
		if (inputHash.containsKey(fullname)){
			fis = inputHash.get(fullname);
			System.out.println("Server: download " + fullname + " inputstream exist in map!");
		}else{
			fis = new FileInputStream(f);
			inputHash.put(fullname, fis);
			
			if (Server.filesVersion.get(f.getAbsolutePath()) == null){
				Server.filesVersion.put(f.getAbsolutePath(), 0);
			}
			
			System.out.println("Server: download " + fullname + " inputstream not exist in map!");
		}
		
		byte[] buf = new byte[Server.BUF_SIZE];
		int byteread = 0;
		
		if ((byteread = fis.read(buf)) != -1){
			byte[] result = Arrays.copyOf(buf, byteread);
			DownloadFormat temp = new DownloadFormat(downloadID, result);
			
			if (byteread < Server.BUF_SIZE){
				/* this is the final part */
				temp.isFinalPart = true;
				tempFile.delete();
				fis.close();
				inputHash.remove(fullname);
			}
			
			return temp;
		}else{
			tempFile.delete();
			fis.close();
			inputHash.remove(fullname);
			return null;
		}
	}
	
	
	/* Stop downloading. It is used when error happens */
	public void stopDownload(int clientID, String filename) throws IOException{
		/* enable proxy to stop downloading when exception happens */
		File f = new File(Server.rootDir, filename + "@" + clientID);
		String fullname = f.getAbsolutePath();
		
		if (inputHash.containsKey(fullname)){
			inputHash.get(fullname).close();
			inputHash.remove(fullname);
			f.delete();
		}
	}

	/* client would call this methods to upload file*/
	public synchronized void upload(int clientID, String filename, byte[] buf) throws IOException{
		
		while (clientID != uploadingClientID){
			if (uploadingClientID == -1){
				uploadingClientID = clientID;
			}else{
				try {
					wait();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
		
		File f = new File(Server.rootDir, filename + "_W_TEMP_@_" + clientID);
		String fullname = f.getAbsolutePath();
		FileOutputStream fos; 
		
		if (outputHash.containsKey(fullname)){
			fos = outputHash.get(fullname);
			System.out.println("Server: upload " + fullname + "outputstream exist!");
		}else{
			fos = new FileOutputStream(f);
			outputHash.put(fullname, fos);
			System.out.println("Server: upload " + fullname + "outputstream not exist in map!");
		}
		
		if (buf == null){
			fos.close();
			outputHash.remove(fullname);
			/* replace the original file with new file*/
			File old = new File(Server.rootDir, filename);
			old.delete();
			f.renameTo(old);
			updateServerCopyVersion(filename);
			uploadingClientID = -1;
			notify();
		}else{
			fos.write(buf);
		}
	}
	
	/* delete file in server */
	public int deleteFile(String filename) throws IOException{
		File f = new File(Server.rootDir, filename);
		
		if (!f.exists()){
			return -1;
		}else if (f.isDirectory()){
			return -2;
		}else{
			updateServerCopyVersion(filename);
			f.delete();
			return 0;
		}
	}
	
}
