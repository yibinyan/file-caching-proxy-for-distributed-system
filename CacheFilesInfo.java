import java.io.File;
import java.nio.channels.FileChannel;


public class CacheFilesInfo {
	public String filePath;
	public FileChannel fileChannel;
	public boolean isOpenForWrite;
	
	public CacheFilesInfo(String filePath, FileChannel fileChannel, boolean isOpenForWrite){
		this.filePath = filePath;
		this.fileChannel = fileChannel;
		this.isOpenForWrite = isOpenForWrite;
	}
}
