
import java.io.*;
import java.net.MalformedURLException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.NonWritableChannelException;
import java.nio.file.Files;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Map;

/*
 * Most behavior and implemenetation 
 * of Proxy is defined in this Class. 
 * 
 * */

class Proxy {
	private static int nextFd;  /* next fd to be allocated */
	private static int nextClientID; /* next client ID to be allocated */
	private static String serverIP; /* IP of server */
	private static String serverPort; /* Port of server */
	public static String cacheDir; /* root directory of proxy*/
	public static long cacheSize;  /* max size of cache */
	public static String url;  /* url of server RMI */
	public static LRUCache cache; /* LRU cache */
	
	/* Allocate unique ID for clients */
	public static synchronized int createClientID(){
		int clientID = nextClientID;
		nextClientID++;
		return clientID;
	}
	
	/* Allocate unique fd for each opening file */
	public static synchronized int createFd(){
		int fd = nextFd;
		nextFd++;
		return fd;
	}
	
	private static class FileHandler implements FileHandling {
		
		public Hashtable<Integer, OpeningFileInfo> fileInfos;
		public ArrayList<Integer> dirFds;
		public ServerFile serverFile;
		public ProxyFileHelper helper;
		public int clientID;
	
		public FileHandler(){
			fileInfos =  new Hashtable<Integer, OpeningFileInfo>();
			dirFds = new ArrayList<Integer>();
			clientID = createClientID();
					
			try {
				serverFile = (ServerFile) Naming.lookup(url);
				helper = new ProxyFileHelper(serverFile);
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (RemoteException e) {
				e.printStackTrace();
			} catch (NotBoundException e) {
				e.printStackTrace();
			}
		}
		
		
		/* open file */
		public int open( String path, OpenOption o ) {
			
			System.out.println("<--------------------OPEN: " +  path + " --------------------->");
			
			
			File f = new File(cacheDir, path);
			
			try {
				String canonicalPath = f.getCanonicalPath();
				if (!canonicalPath.contains(cacheDir)){
					/* illegal filename */
					return ProxyFileHelper.EACCES;
				}else{
					path = canonicalPath.substring(cacheDir.length());
					f = new File(cacheDir, path);
					System.out.println("Standard path: " + path);
				}
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			
			String option = null;
			boolean isFileExist = f.exists();
			boolean isDir = f.isDirectory();
			boolean isVersionValid = false;
			boolean isOpenForWrite = false;
			
			if (isFileExist){
				/* check if the local file is stale */
				if (cache.get(path) == null){
					System.out.println("OPEN: " +  path + " NULL pointer!");
				}
				
				isVersionValid = helper.checkVersion(path, 
									cache.get(path).fileVersion);
				System.out.println("[Proxy] File exist ");
				if (!isVersionValid){
					/* file stale should keep another copy if it is still opened by other */
					CacheFileNode node = cache.get(path);
					System.out.println("[Proxy] File stale, open count " + node.openCount);
					if (node.openCount != 0){
						String statleName = path + "_stale_" + cache.get(path).fileVersion;
						cache.changeNameAndTurnStale(path, statleName);
						
						File orignal = new File(cacheDir, path);
						File staleFile = new File(cacheDir, statleName);
						orignal.renameTo(staleFile);
						
						/* modify Opening fileinfo */
						for (OpeningFileInfo v : fileInfos.values()){
							if (v.filePath.equals(path)){
								v.filePath = statleName;
								break;
							}
						}
						
					}else{
						Proxy.cache.deleteFromCache(path);
					}
					
					isFileExist = false;
				}else{
					/* increase the number of client that open this file */
					Proxy.cache.get(path).increaseOpenCount();
				}
			}
			
			/* file is not in the cache or file is stale, try fetch from server */
			if ((!isFileExist) || (!isVersionValid)){
				int result = helper.downloadAndUpdate(clientID, path);
				if (result >= 0){
					System.out.println("[Open] server version" + result);
					/* downloadAndUpdate returns the version of server copy */
					if (!isFileExist){
						/* stale version is not exist, 
						/* add new file to cache  */
						System.out.println("[TEST] ADD " + path + "To Cache");
						Proxy.cache.addToCache(path, f.length(), result);
						
						/* update version at open */
						Proxy.cache.get(path).fileVersion = result;
						/* increase the number of client that open this file */
						Proxy.cache.get(path).increaseOpenCount();
					}
					
					isFileExist = true;					

				}else{
					if (result == ProxyFileHelper.FILENOTFOUND_ERROR){
						/* file is not exist in server*/
						System.out.println("[Open] file is not exist in server ");
						isFileExist = false;
					}else if (result == ProxyFileHelper.EFBIG){
						System.out.println("[Open Error] Remote file is larger than cache size");
						/* EFBIG == 27 file too big */
						return ProxyFileHelper.EFBIG;
					}else if (result == ProxyFileHelper.EIO){
						System.out.println("[Open Error] Download Error");
						/* EIO == 5  I/O error*/
						return ProxyFileHelper.EIO;
					}
				}
			}
			
			switch(o){
				case READ: 
					if (isFileExist){
						option = "r";
						isOpenForWrite = false;
					}else{
						return Errors.ENOENT;
					}
					break;
				
				case WRITE: 
					if (isFileExist){
						if (!isDir){
							option = "rw";
							isOpenForWrite = true;
						}else{
							return Errors.EISDIR;
						}
					}else{
						return Errors.ENOENT;
					}
					break;
				
				case CREATE: 
					if (isDir){
						/* will not create directory */
						return Errors.EISDIR;
					}else if (path.charAt(path.length()-1) == '/'){
						/* path name is a directory */
						return Errors.ENOENT;
					}
					
					option = "rw";
					isOpenForWrite = true;
					
					break;
					
				case CREATE_NEW:
					if (!isFileExist){
						if (path.charAt(path.length()-1) == '/'){
							/* path name is a directory */
							return Errors.ENOENT;
						}

						option = "rw"; 
						isOpenForWrite = true;
					}else{
						return Errors.EEXIST;
					}
					break;
			}
			
			int currentFd = createFd();
			
			if (isDir){
				/* when open a directory use RDONLY*/
				dirFds.add(currentFd);
				return currentFd;
			}
			
			RandomAccessFile raFile = null;
			
			try {
				raFile = new RandomAccessFile(f, option);
				
				if (isOpenForWrite){
					/* Open for write, should create another copy */
					int count = 0;
					String prifix = path + "_private_";
					String orginalPath = path;
					String privatePath;
					
					
					File privateCopy = new File(cacheDir, prifix + count);
					while (privateCopy.exists()){
						/* find a unique name */
						count++;
						privateCopy = new File(cacheDir, prifix + count);
					}
					
					privatePath = prifix + count;
					raFile.close();
					
					while (cache.remainSize < f.length()){
						long releasedSize = Proxy.cache.evict();
						
						if (releasedSize < 0){
							/* cache is too small to held private copy */
							if (!isFileExist){
								/* original file is not exist, then we should only keep private copy */
								System.out.println("[Makeing private copy] orignal file not exist");
								f.delete();
							}else{
								System.out.println("[Makeing private copy] orignal file is exist");
								/* orignal file is exist, then update cache since it is used */
								Proxy.cache.get(orginalPath).decreaseOpenCount();
								Proxy.cache.update(orginalPath, f.length());
							}
						
							System.out.println("Cache is too small to store private copy");
							return ProxyFileHelper.EFBIG;
						}
					}
					
					Files.copy(f.toPath(), privateCopy.toPath());
						
					if (privateCopy.exists()){
						System.out.println("[Private copy created!!]");
					}
					
					/* put private copy into cache */
					Proxy.cache.addToCache(privatePath, privateCopy.length(), 0);
					raFile = new RandomAccessFile(privateCopy, option);
					
					path = privatePath;
					
					if (!isFileExist){
						System.out.println("[Makeing private copy] orignal file not exist");
						/* original file is not exist, then we should only keep private copy */
						f.delete();
					}else{
						System.out.println("[Makeing private copy] orignal file is exist");
						/* orignal file is exist, then update cache since it is used */
						Proxy.cache.get(orginalPath).decreaseOpenCount();
						Proxy.cache.update(orginalPath, f.length());
					}
				}
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			OpeningFileInfo cfi 
				= new OpeningFileInfo(path, raFile.getChannel(), isOpenForWrite);
			fileInfos.put(currentFd, cfi);
			
			System.out.print("OPEN ");
			cache.printCache();
			
			return currentFd;
		}

		/* close file */
		public int close( int fd ) {
			OpeningFileInfo cfi = fileInfos.get(fd);
			FileChannel fc = cfi.fileChannel;
			Boolean isDirfd = dirFds.contains(fd);
			String filename = cfi.filePath;
			
			if (isDirfd){
				dirFds.remove(new Integer(fd));
			}

			if (fc == null){
				return Errors.EBADF;
			}else{
				try {
					fc.close();
					if (cfi.isOpenForWrite){
						helper.uploadPrivateCopy(clientID, cfi.filePath);
						cache.deleteFromCache(cfi.filePath);
					}else{
						System.out.println("[Close + " + filename + "] update ");
						File f = new File(Proxy.cacheDir, cfi.filePath);
						CacheFileNode node = cache.get(cfi.filePath);
						
						if (node != null){
							node.decreaseOpenCount();

							if ((node.isStale) && (node.openCount == 0)){
								/* delete stale version */
								cache.deleteFromCache(cfi.filePath);
							}else{
								/* update cache when close */
								cache.update(cfi.filePath, f.length());
							}
						}
					}
					
					fileInfos.remove(fd);
					
				} catch (IOException e) {
					e.printStackTrace();
					/* I/O error */
					return ProxyFileHelper.EIO;
				}
			}
			
			System.out.print("[CLOSE " + filename + " ] ");
			cache.printCache();
			/* if success return 0*/
			return 0;
		}

		/* write to file */
		public long write( int fd, byte[] buf ) {
			FileChannel fc = fileInfos.get(fd).fileChannel;
			Boolean isDirfd = dirFds.contains(new Integer(fd));			
			
			if (isDirfd){
				/* will not write to directory */
				return Errors.EBADF;
			}
			
			if (fc == null){
				return Errors.EBADF;
			}
			if (buf == null){
				return Errors.EINVAL;
			}
			
			while (buf.length > Proxy.cache.remainSize){
				long releasedSize = Proxy.cache.evict();
				if (releasedSize < 0){
					File f = new File(cacheDir, fileInfos.get(fd).filePath);
					
					/* cache is empty, buf is too large */
					System.out.println("[Error] Writing lead to file larger than cache size");
					System.out.println("[Error] filesize: " + f.length());
					
					/* EFBIG happen when create new file, cancel creating this file */
					try {
						fc.close();
						cache.deleteFromCache(fileInfos.get(fd).filePath);
					} catch (IOException e) {
						e.printStackTrace();
					}
					
					return ProxyFileHelper.EFBIG;
				}
			}
			
			ByteBuffer buffer = ByteBuffer.allocate(buf.length);
			buffer.clear();
			buffer.put(buf);
			buffer.flip();
			
			long noOfBytesWritten = 0;
			
			try {
				while (buffer.hasRemaining()){
					noOfBytesWritten += fc.write(buffer);
				}
				
			} catch (NonWritableChannelException e) {
				return Errors.EBADF;
			} catch (IOException e){
				e.printStackTrace();
				/* I/O error */
				return ProxyFileHelper.EIO;
			}
			
			return noOfBytesWritten;
		}

		/* read from file */
		public long read( int fd, byte[] buf ) {
			FileChannel fc = fileInfos.get(fd).fileChannel;
			Boolean isDirfd = dirFds.contains(new Integer(fd));	
			
			if (isDirfd){
				/* it is directory */
				return Errors.EISDIR;
			}
			
			if (fc == null){
				return Errors.EBADF;
			}
			if (buf == null){
				return Errors.EINVAL;
			}
			
			ByteBuffer buffer = ByteBuffer.allocate(buf.length);
			int bytesRead = 0;

			try {
				bytesRead = fc.read(buffer);
				
				if (bytesRead == -1){
					/* end of file */
					bytesRead = 0;
				}
				
			} catch (IOException e) {
				e.printStackTrace();
				/* I/O error */
				return ProxyFileHelper.EIO;
			}
			
			buffer.rewind();
			buffer.get(buf);
			buffer.rewind();
			
			return bytesRead;
		}

		
		/* move the pointer of file */
		public long lseek( int fd, long pos, LseekOption o ) {
			FileChannel fc = fileInfos.get(fd).fileChannel;
			
			if (fc == null){
				return Errors.EBADF;
			}
			
			long filesize = 0;
			long currentPos = 0;
			long futurePos = 0;
			
			try {
				filesize = fc.size();
				currentPos = fc.position();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			switch(o){
				case FROM_CURRENT:
					futurePos = currentPos + pos;
					break;
					
				case FROM_END:
					futurePos = filesize + pos;
					break;
					
				case FROM_START:
					futurePos = pos;
					break;
			}
			
			
			if ((pos < 0) && (futurePos < 0)){
				return Errors.EINVAL;
			}else if ((pos > 0) && (futurePos < 0)){
				/* overflow */
				return ProxyFileHelper.EOVERFLOW;
			}else{
				try {
					fc.position(futurePos);
				} catch (IOException e) {
					e.printStackTrace();
					/* I/O error */
					return ProxyFileHelper.EIO;
				}
			}
			
			
			return futurePos;
		}

		/* delete file */
		public int unlink(String path) {
			File f = new File(cacheDir, path);
			
			try {
				String canonicalPath = f.getCanonicalPath();
				if (!canonicalPath.contains(cacheDir)){
					/* illegal filename */
					return ProxyFileHelper.EACCES;
				}else{
					path = canonicalPath.substring(cacheDir.length());
				}
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			
			int result = helper.deleteServerFile(path);
			
			Boolean isFileExist = true;
			Boolean isDir = false;
			
			if (result == -1){
				isFileExist = false;
			}
			
			if (result == -2){
				isDir = true;
			}

			if (!isFileExist){
				return Errors.ENOENT;
			}
			
			if (isDir){
				return Errors.EPERM;
			}
			
            
			System.out.print("DELETE ");
			Proxy.cache.printCache();
            
	        /* success */
			return 0;
		}

		/* do some clean up when done */
		public void clientdone() {
			
			for (OpeningFileInfo cfi : fileInfos.values()){
				try {
					cfi.fileChannel.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			
			fileInfos.clear();
			return;
		}

	}
	
	private static class FileHandlingFactory implements FileHandlingMaking {
		public FileHandling newclient() {
			return new FileHandler();
		}
	}

	public static void main(String[] args) throws IOException {
		System.out.println("Hello World");

		if (args.length != 4){
			System.out.println("Proxy Argument Error");
			return;
		} 
		
		nextFd = 5;	/* fd start with 5 */
		serverIP = args[0];
		serverPort = args[1];
		cacheDir = new File(args[2]).getCanonicalPath();
		cacheSize = Long.parseLong(args[3]);
		url = "rmi://" + serverIP + ":" + serverPort + "/server";
		cache = new LRUCache();

		System.out.println("URL = " + url);
		
		RPCreceiver receiver = new RPCreceiver(new FileHandlingFactory());
		new Thread(receiver).start();
	}
}

