import java.io.Serializable;

/*
 * This is a class that server would return as a return value 
 * to the client when client ask to download file
 * 
 * */
public class DownloadFormat implements Serializable {
	String downloadID; /* download ID that server allocate to client */	 
	byte[] datas; /* partical data of target file */
	boolean isFinalPart; /* true if thie part of data is final part of target file */
	
	public DownloadFormat(String downloadID, byte[] datas){
		this.downloadID = downloadID;
		this.datas = datas;
		isFinalPart = false;
	}
}
