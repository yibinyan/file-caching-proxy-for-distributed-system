/*
 * This class is the states of cached file 
 * which would be add to the LRUcache
 */
public class CacheFileNode {
	String fileName; /* name of file */
	long fileSize; /* size of file */
	CacheFileNode next; /* node next to current node */
	CacheFileNode before; /* node before current node */
	int fileVersion; /* version of cached file */
	int openCount; /* number of clients who are opening this file */
	boolean isStale; /* whether this file is outdated */
	
	public CacheFileNode(){
		this.fileName = null;
		this.fileSize = 0;
		this.fileVersion = 0;
		this.openCount = 0;
		this.next = null;
		this.before = null;
		this.isStale = false;
	}
	
	public CacheFileNode(String fileName, long fileSize, int fileVersion){
		this.fileName = fileName;
		this.fileSize = fileSize;
		this.fileVersion = fileVersion;
		this.next = null;
		this.before = null;
		this.isStale = false;
	}
	
	/* add node to the before of this node */
	public void addToBefore(CacheFileNode beforeNode){
		beforeNode.before = this.before;
		this.before = beforeNode;
		beforeNode.next = this;
	}
	
	/* add node to the next of this node */
	public void addToNext(CacheFileNode nextNode){
		nextNode.next = this.next;
		this.next = nextNode;
		nextNode.before = this;
	}
	
	/* delete next node */
	public void deleteNext(){
		if (this.next != null){
			CacheFileNode nextNode = this.next;
			CacheFileNode nextNextNode = nextNode.next;
			this.next = nextNextNode;
			/* in case of nextNextNode is null*/
			if (nextNextNode != null){
				nextNextNode.before = this;
			}
			
			/* clear pointer in the next node */
			nextNode.next = null;
			nextNode.before = null;
		}
	}
	
	/* delete before node */
	public void deleteBefore(){
		if (this.before != null){
			CacheFileNode beforeNode = this.before;
			CacheFileNode beforeBeforeNode = beforeNode.before;
			this.before = beforeBeforeNode;
			/* in case of beforeBeforeNode is null*/
			if (beforeBeforeNode != null){
				beforeBeforeNode.next = this;
			}
			
			/* clear pointer in the next node */
			beforeNode.next = null;
			beforeNode.before = null;
		}
	}
	
	/* increase the count the clients who is opening this file */
	public void increaseOpenCount(){
		this.openCount++;
	}
	
	/* decrease the count the clients who is opening this file */
	public void decreaseOpenCount(){
		this.openCount--;
	}
	
}
